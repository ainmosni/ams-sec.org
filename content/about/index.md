---
title: "About me"
date: 2022-01-10T20:17:40+01:00
---

Hi, I'm Daniël Franke, also known as ainmosni.

I'm a Berliner, originally hailing from Amsterdam, having moved to Berlin
quite a few years ago.

I've been doing things with technology for as long as I remember,
and professionally since around the start of the millennium. As might be
expected, I've picked up knowledge, and opinions in that time, and this is my
corner of the internet to share that knowledge, and those opinions.

But the articles on this blog won't be limited to just technology, as this
is my corner of the internet, I will write about politics, world events,
culture, both popular and less-popular, and whatever I want.